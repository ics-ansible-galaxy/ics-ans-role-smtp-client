import os
import testinfra.utils.ansible_runner


testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


def test_postfix_config(host):
    postfix = host.file("/etc/postfix/main.cf")
    assert postfix.contains("mydomain")
    assert postfix.user == "root"
    assert postfix.group == "root"


def test_postfix_is_installed(host):
    postfix = host.package("postfix")
    assert postfix.is_installed


def test_postfix_running_and_enabled(host):
    postfix = host.service("postfix")
    assert postfix.is_running
    assert postfix.is_enabled
