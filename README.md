# ics-ans-role-smtp-client

Ansible role to install Postfix and configure it as an smtp client.

## Role Variables

```yaml
# Specify DNS domain (email address suffix)
smtp_relay_domain: domain.local
# Specify an SMTP relay host
smtp_relay_server: localhost
# Set the maximum email body size in bytes
smtp_relay_msg_size: 102400000
...
```

## Example Playbook

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-smtp-client
```

## License

BSD 2-clause
